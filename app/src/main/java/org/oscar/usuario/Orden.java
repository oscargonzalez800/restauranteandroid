package org.oscar.usuario;

/**
 * Created by oscar on 16/03/2015.
 */
public class Orden {
    public String getBebidaFria() {
        return bebidaFria;
    }

    public void setBebidaFria(String bebidaFria) {
        this.bebidaFria = bebidaFria;
    }

    public String getBebidaCaliente() {
        return bebidaCaliente;
    }

    public void setBebidaCaliente(String bebidaCaliente) {
        this.bebidaCaliente = bebidaCaliente;
    }

    public String getPostre() {
        return postre;
    }

    public void setPostre(String postre) {
        this.postre = postre;
    }

    public Orden(String bebidaFria, String bebidaCaliente, String postre) {
        this.bebidaFria = bebidaFria;
        this.bebidaCaliente = bebidaCaliente;
        this.postre = postre;
    }

    private String bebidaFria;
    private String bebidaCaliente;
    private String postre;



}
