package org.oscar.usuario;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.view.Menu;
import com.example.oscar.restaurante.R;

import java.util.ArrayList;
import java.util.List;


public class AdaptadorUsuario extends ArrayAdapter<Usuario> {

private Usuario[] datos;
    List<Usuario> Usuario = new ArrayList<Usuario>();

public AdaptadorUsuario(Context context, Usuario[] datos) {
        super(context, R.layout.activity_main, datos);
        this.datos = datos;
        }

public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.activity_main, null);

        TextView lblNombre = (TextView)item.findViewById(R.id.lblNombre);
        lblNombre.setText(datos[position].getNombre());

        TextView lblApellido = (TextView)item.findViewById(R.id.lblApellido);
        lblApellido.setText(datos[position].getApellido());

        Usuario usuario = Usuario.get(position);

        TextView nombre = (TextView) convertView.findViewById(R.id.txtUsuarioRegistrar);
        nombre.setText(usuario.getNombre());

        TextView pass = (TextView) convertView.findViewById(R.id.txtPasswordRegistrar);
        pass.setText(usuario.getPass());
        TextView apellido = (TextView) convertView.findViewById(R.id.txtApellidoRegistrar);
        apellido.setText(usuario.getApellido());

        return (item);
        }
        }