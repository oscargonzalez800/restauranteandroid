package com.example.oscar.restaurante;

import android.content.res.Resources;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.oscar.usuario.Usuario;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    List<Usuario> Usuario = new ArrayList<Usuario>();
    static TabHost mytabs;
  private   ListView usuarioListView;
    EditText txtUsuario,txtpass,txtapellido,txtUsuarioLogin,txtPassLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Resources recources = getResources();

        TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);
        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("Registrar");
        tabSpec.setContent(R.id.Registrar);
        tabSpec.setIndicator("Tab1");
        tabHost.addTab(tabSpec);

        tabSpec= tabHost.newTabSpec("Inicio");
        tabSpec.setContent(R.id.Inicio);
        tabSpec.setIndicator("Tab2");
        tabHost.addTab(tabSpec);

        tabSpec= tabHost.newTabSpec("Orden");
        tabSpec.setContent(R.id.Orden);
        tabSpec.setIndicator("Tab3");
        tabHost.addTab(tabSpec);






        /*tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
            @Override

            public void onTabChanged(String tabId){
                Log.i("AndroidTab")
            }
        });*/
        int position;

        txtUsuario = (EditText)findViewById(R.id.txtUsuarioRegistrar);
        txtpass = (EditText)findViewById(R.id.txtPasswordRegistrar);
        txtapellido=(EditText)findViewById(R.id.txtApellidoRegistrar);


        final Button btnMenu = (Button) findViewById(R.id.btnInicio);
        final Button btnOrden = (Button) findViewById(R.id.btnIngresar);
        final Button btnMostrar = (Button) findViewById(R.id.btnVer);

        btnOrden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);
                tabHost.setup();
                tabHost.setCurrentTab(2);
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);
                tabHost.setup();
                tabHost.setCurrentTab(1);
            }
        });




        btnMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);
                tabHost.setup();
                tabHost.setCurrentTab(3);
            }
        });




        final Button btnAgregarUsuario = (Button) findViewById(R.id.btnRegistrar);
        btnAgregarUsuario.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                AgregarUsuario(txtUsuario.getText().toString(),txtpass.getText().toString(),txtapellido.getText().toString());
                Toast.makeText(getApplicationContext(),""+R.id.txtUsuarioRegistrar,Toast.LENGTH_SHORT);
                TextView lblNombre = (TextView)findViewById(R.id.lblNombre);
                lblNombre.setText((txtUsuario.getText()));
                TextView lblApellido = (TextView)findViewById((R.id.lblApellido));
                lblApellido.setText(txtUsuario.getText());
                   TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);
                tabHost.setup();
                tabHost.setCurrentTab(1);



            }
        });

    }

    private  void AgregarUsuario(String Nombre , String Apellido , String Pass){
        Usuario.add(new Usuario(Nombre,Pass,Apellido));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
